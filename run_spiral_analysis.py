#!/usr/bin/env python

"""
Script performs the analysis of the spiral structure:
1) it calls 'connect_with_spirals' script to create a regions file with tangents to spirals
2) it calls 'cut_spirals' script to make spiral cuts and fit them
"""

import argparse
from pathlib import Path
from os import remove
from os import makedirs
from os import path
import subprocess
import shutil
from astropy.io import fits
from libs.make_1d_psf import make_1d_psf
from libs.get_noise import get_noise
from libs.connect_with_spirals import connect_with_spirals
from libs.make_cuts import make_cuts
from libs.cut_tails import cut_tails
from libs.smooth_spirals import smooth_spirals
from libs.analyse_results import analyse_results


def rm(file_name):
    if Path(file_name).exists():
        remove(file_name)


def mv(src, dst):
    if Path(src).exists():
        shutil.move(src, dst)
    else:
        print("%s does not exist" % src)


def run(args):
    outdir = Path(str(args.datapath) + "_results")
    # Find image in the data directory
    image_name = list(args.datapath.glob("*.phot.*nonan*deprojected.fits"))[0]
    galname = image_name.name.split(".")[0]
    print(f"Galaxy image: {image_name}")
    # Find mask file
    mask_name = list(args.datapath.glob("*newmask.fits"))[0]
    print(f"Mask file: {mask_name}")
    # A file with arms pointing
    arm_pointing_name = args.datapath / "arms.reg"
    # Find galaxy center using center.reg file:
    center_reg_name = list(args.datapath.glob("*.center.reg"))[0]
    for line in open(center_reg_name):
        if line.startswith("point("):
            coords = line[line.index("(")+1: line.index(")")]
            xcen = float(coords.split(",")[0])
            ycen = float(coords.split(",")[1])

    # Find noise parameters
    back_rms = get_noise(image_name, args.datapath / "back.reg")
    # Find galaxy scale
    for line in open("data/scales.dat"):
        if line.startswith("#"):
            continue
        if line.split()[0] == galname:
            galscale = float(line.split()[1])

    # Compute pc per pixel scale
    pc_per_pix = 1.2233 * galscale
    # Create a psf slice
    psf_fwhm = make_1d_psf("data/psf.fits", "psf_1d.dat", 1)
    print(f"psf fwhm: {psf_fwhm}")

    # Load cropping region
    crop_reg_name = list(args.datapath.glob("*.cut.reg"))[0]
    for line in open(crop_reg_name):
        if line.startswith("box"):
            crop_box_params = line[line.index("(")+1: line.index(")")].split(",")
            crop_x_cen = int(float(crop_box_params[0]))
            crop_y_cen = int(float(crop_box_params[1]))
            crop_width = int(float(crop_box_params[2]))
            crop_height = int(float(crop_box_params[3]))
            x_shift = - crop_x_cen + crop_width / 2
            y_shift = - crop_y_cen + crop_height / 2

    if not args.keep_old:
        if path.exists(outdir):
            ans = input("Remove existing [y/N]?").strip().lower()
            if ans == "y":
                shutil.rmtree(outdir)
            else:
                exit()
    if not path.exists(outdir):
        makedirs(outdir)

    # Crop galaxy image
    galaxy_data_full = fits.getdata(image_name)
    galaxy_data_cropped = galaxy_data_full[int(crop_y_cen-crop_height/2): int(crop_y_cen+crop_height/2),
                                           int(crop_x_cen-crop_width/2): int(crop_x_cen+crop_width/2)]
    fits.PrimaryHDU(data=galaxy_data_cropped).writeto("image_cropped.fits", overwrite=True)
    xcen = xcen + x_shift
    ycen = ycen + y_shift
    # Crop mask image
    mask_data_full = fits.getdata(mask_name)
    mask_data_cropped = mask_data_full[int(crop_y_cen-crop_height/2): int(crop_y_cen+crop_height/2),
                                       int(crop_x_cen-crop_width/2): int(crop_x_cen+crop_width/2)]
    fits.PrimaryHDU(data=mask_data_cropped).writeto("mask_cropped.fits", overwrite=True)

    # Fix coordinates in the arm crop file
    fout = open("arms_cropped.reg", "w")
    for line in open(arm_pointing_name):
        if line.startswith("circle"):
            circ_params = line[line.index("(")+1: line.index(")")].split(",")
            circ_x = float(circ_params[0]) + x_shift
            circ_y = float(circ_params[1]) + y_shift
            out_line = f"circle({circ_x},{circ_y},{line.split(',')[-1]}"
            fout.write(out_line)
    fout.close()

    # 1) Make tangents
    connect_with_spirals("arms_cropped.reg", xcen, ycen, "arm_centers.reg", "arm_tangents.reg")

    # 2) Make cuts
    make_cuts("image_cropped.fits", "arm_tangents.reg", xcen, ycen, outdir,
              "psf_1d.dat", psf_fwhm, "mask_cropped.fits", pc_per_pix, args.width)

    # 2b) Cut weak tails of spirals
    cut_tails(outdir, back_rms, 1)

    # 3) Smooth spirals
    smooth_spirals("image_cropped.fits", outdir, xcen, ycen, pc_per_pix)

    # 3b) Make spiral mask Do we really need this?
    # call_string = "./make_spirals_mask.py --data %s " % (outdir / args.passband)
    # call_string += "--xcen %1.3f --ycen %1.3f " % (deproj_x_cen, deproj_y_cen)
    # call_string += "--ell 0.0 --posang 0.0"
    # call_string += " --galname %s" % args.passband
    # print(call_string)
    # subprocess.call(call_string, shell=True)

    # 4) Create model arms Do we really need this
    # call_string = "./model_arms.py --data %s " % (outdir / args.passband)
    # call_string += "--xcen %1.3f --ycen %1.3f " % (deproj_x_cen, deproj_y_cen)
    # subprocess.call(call_string, shell=True)

    # 5) Perform some analysis of the results
    analyse_results(outdir, xcen, ycen, pc_per_pix)

    # Cleanup
    mv("arm_tangents.reg", outdir / "arm_tangents.reg")
    mv("arm_centers.reg", outdir / "arm_centers.reg")
    mv("psf_1d.dat", outdir / "psf_1d.dat")
    mv("image_cropped.fits", outdir / "image_cropped.fits")
    mv("mask_cropped.fits", outdir / "mask_cropped.fits")
    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--datapath", type=Path, help="Path to a directory with galaxy files")
    parser.add_argument("--width", type=float, help="Slice width (in units of slice lenght), default=0.2",
                        default=0.2)
    parser.add_argument("--keep-old", action="store_true", default=False)
    args = parser.parse_args()

    res = run(args)
