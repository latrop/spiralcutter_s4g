#! /usr/bin/env python

from math import sin
from math import cos
from math import exp
import math
import numpy as np
from scipy.interpolate import interp1d


def polar_to_cartesian(polar_coords, x_orig, y_orig):
    x_list = []
    y_list = []
    for polarPoint in polar_coords:
        r = polarPoint[0]
        phi = polarPoint[1]
        x = r * cos(phi) + x_orig
        y = r * sin(phi) + y_orig
        x_list.append(x)
        y_list.append(y)
    return list(zip(x_list, y_list))


def cartesian_to_polar_plain(x, y, xcen=0, ycen=0):
    """
    A straightforward cartesian to polar converter
    """
    dx = x - xcen
    dy = y - ycen
    r = math.hypot(dx, dy)
    phi = math.atan2(dy, dx)
    # put phi in range [0, 2pi] instead of [-pi, pi]
    if phi < 0:
        phi += 2 * math.pi
    return r, phi


def cartesian_to_polar_spiral(x_spiral, y_spiral, xcen, ycen, winding, allow_non_monotony=False):
    """
    Converts spiral coordinates from cartesian to polar coordinates taking
    into account winding direction to address 2*pi*k ambiguity problem
    """
    r, phi = cartesian_to_polar_plain(x_spiral[0], y_spiral[0], xcen, ycen)
    r_list, phi_list, good_inds = [r], [phi], [0]
    for idx in range(1, len(x_spiral)):
        r, phi = cartesian_to_polar_plain(x_spiral[idx], y_spiral[idx], xcen, ycen)
        if not allow_non_monotony and (r < r_list[-1]):
            continue
        if winding == "counterclockwise":
            # Take into account the possibility of several revolutions of the spiral arount the centre
            while phi < phi_list[-1]:
                phi += 2 * math.pi
        elif winding == "clockwise":
            while phi > phi_list[-1]:
                phi -= 2 * math.pi
        if abs(phi - phi_list[-1]) > 1.5*math.pi:
            print("skipping")
            continue
        phi_list.append(phi)
        r_list.append(r)
        good_inds.append(idx)
    return np.array(r_list), np.array(phi_list), good_inds


def make_log_spirals(spiral_locations_sparse, widths):
    """ Function takes points along the spiral (in polar coordinate system)
    and adds some additional points between them based on approximating
    the curve by logarithmic spirals"""
    r_sparse, phi_sparse = zip(*spiral_locations_sparse)
    r_smooth = []
    phi_smooth = []
    lambda_smooth = []  # Equal to tangent of pitch angle
    width_smooth = []
    r0_smooth = []
    phi0_smooth = []
    if len(r_sparse) >= 2:
        for j in range(1, len(r_sparse)):
            # Compute spiral arm segment parameters
            # tangent of the pitch angle
            lam = (math.log(r_sparse[j]) - math.log(r_sparse[j-1])) / (phi_sparse[j] - phi_sparse[j-1])
            r0 = r_sparse[j-1]      # Starting values of phi and t
            phi0 = phi_sparse[j-1]  #
            if phi_sparse[j-1] < phi_sparse[j]:
                phiNew = np.arange(phi_sparse[j-1], phi_sparse[j], 3.1/r0)
            else:
                phiNew = np.arange(phi_sparse[j-1], phi_sparse[j], -3.1/r0)
            rNew = r0 * np.exp(lam*(phiNew-phi0))

            # compute interpolated width
            width_start = widths[j-1]
            width_end = widths[j]
            widths_interpolated = interp1d(x=[phi_sparse[j-1], phi_sparse[j]],
                                           y=[width_start, width_end], kind='linear')

            # Pack the results
            r_smooth.extend(rNew)
            phi_smooth.extend(phiNew)
            lambda_smooth.extend(np.zeros_like(rNew) + lam)
            width_smooth.extend(widths_interpolated(phiNew))
            r0_smooth.extend(np.zeros_like(rNew) + r0)
            phi0_smooth.extend(np.zeros_like(rNew) + phi0)

        # Add some points in the end
        last_x = r_smooth[-1] * np.cos(phi_smooth[-1])
        last_y = r_smooth[-1] * np.sin(phi_smooth[-1])
        delta_phi = phi_smooth[-1] - phi_smooth[-2]
        while 1:
            phiNew = phi_smooth[-1] + delta_phi
            rNew = r0 * np.exp(lambda_smooth[-1]*(phiNew-phi0))
            x = rNew * np.cos(phiNew)
            y = rNew * np.sin(phiNew)
            dist = np.hypot(x-last_x, y-last_y)
            if dist > width_smooth[-1]:
                break
            r_smooth.append(rNew)
            phi_smooth.append(phiNew)
            lambda_smooth.append(lambda_smooth[-1])
            width_smooth.append(width_smooth[-1])
            r0_smooth.append(r0_smooth[-1])
            phi0_smooth.append(phi0_smooth[-1])
        return list(zip(r_smooth, phi_smooth)), lambda_smooth, width_smooth, r0_smooth, phi0_smooth
    else:
        return None


def make_log_spirals_asymm(spiral_locations_sparse, widths_left, widths_right):
    """ Function takes points along the spiral (in polar coordinate system)
    and adds some additional points between them based on approximating
    the curve by logarithmic spirals. The difference between this function
    and the make_log_spirals is that it takes two widths"""
    r_sparse, phi_sparse = zip(*spiral_locations_sparse)
    r_smooth = []
    phi_smooth = []
    lambda_smooth = []  # Equal to tangent of pitch angle
    width_left_smooth = []
    width_right_smooth = []
    r0_smooth = []
    phi0_smooth = []
    if len(r_sparse) >= 2:
        for j in range(1, len(r_sparse)):
            # Compute spiral arm segment parameters
            # tangent of the pitch angle
            if (phi_sparse[j] - phi_sparse[j-1]) == 0:
                continue
            lam = (math.log(r_sparse[j]) - math.log(r_sparse[j-1])) / (phi_sparse[j] - phi_sparse[j-1])
            r0 = r_sparse[j-1]      # Starting values of phi and t
            phi0 = phi_sparse[j-1]  #
            # Fill points
            if phi_sparse[j-1] < phi_sparse[j]:
                phiNew = np.arange(phi_sparse[j-1], phi_sparse[j], 0.1/r0)
            else:
                phiNew = np.arange(phi_sparse[j-1], phi_sparse[j], -0.1/r0)
            rNew = r0 * np.exp(lam*(phiNew-phi0))

            # compute interpolated widths
            width_left_start = widths_left[j-1]
            width_left_end = widths_left[j]
            widths_left_interpolated = interp1d(x=[phi_sparse[j-1], phi_sparse[j]],
                                                y=[width_left_start, width_left_end], kind='linear')

            width_right_start = widths_right[j-1]
            width_right_end = widths_right[j]
            widths_right_interpolated = interp1d(x=[phi_sparse[j-1], phi_sparse[j]],
                                                 y=[width_right_start, width_right_end], kind='linear')

            # Pack the results
            r_smooth.extend(rNew)
            phi_smooth.extend(phiNew)
            lambda_smooth.extend(np.zeros_like(rNew) + lam)
            width_left_smooth.extend(widths_left_interpolated(phiNew))
            width_right_smooth.extend(widths_right_interpolated(phiNew))
            r0_smooth.extend(np.zeros_like(rNew) + r0)
            phi0_smooth.extend(np.zeros_like(rNew) + phi0)
        return list(zip(r_smooth, phi_smooth)), lambda_smooth, width_left_smooth, width_right_smooth,\
            r0_smooth, phi0_smooth
    else:
        return None, None


def make_tangents_to_spirals(spiral_polar_points, lambda_values, width_values, r0_values, phi0_values):
    tangents = []
    for idx in range(len(spiral_polar_points)):
        r0 = r0_values[idx]
        phi0 = phi0_values[idx]
        phi = spiral_polar_points[idx][1]
        lam = lambda_values[idx]
        width = width_values[idx]

        # Compute beginning and ending points of tangents
        a = r0 * exp(lam * (phi - phi0))
        b = (lam * cos(phi) - sin(phi)) / (lam*sin(phi) + cos(phi))
        x_min = a*cos(phi) + cos(math.atan(b)) * width
        x_max = a*cos(phi) - cos(math.atan(b)) * width
        y_min = -b * (x_min - a*cos(phi)) + a * sin(phi)
        y_max = -b * (x_max - a*cos(phi)) + a * sin(phi)
        tangents.append((x_min, y_min, x_max, y_max))
    return tangents


def make_tangents_to_spirals_asymm(spiral_polar_points, lambda_values, width_values_left,
                                   width_values_right, r0_values, phi0_values):
    tangents = []
    for idx in range(len(spiral_polar_points)):
        r0 = r0_values[idx]
        phi0 = phi0_values[idx]
        phi = spiral_polar_points[idx][1]
        lam = lambda_values[idx]
        width_left = width_values_left[idx]
        width_right = width_values_right[idx]

        # Compute beginning and ending points of tangents
        a = r0 * exp(lam * (phi - phi0))
        b = (lam * cos(phi) - sin(phi)) / (lam*sin(phi) + cos(phi))  # This is the slope of the tangent to the spiral
        angle = math.atan(b)
        for angle_shifted in np.linspace(angle-0.1*math.pi, angle+0.1*math.pi, 10):
            b_shifted = math.tan(angle_shifted)
            x_min_1 = a*cos(phi) + cos(math.atan(b_shifted)) * width_left
            x_max_1 = a*cos(phi) - cos(math.atan(b_shifted)) * width_right
            y_min_1 = -b_shifted * (x_min_1 - a*cos(phi)) + a * sin(phi)
            y_max_1 = -b_shifted * (x_max_1 - a*cos(phi)) + a * sin(phi)

            x_min_2 = a*cos(phi) + cos(math.atan(b_shifted)) * width_right
            x_max_2 = a*cos(phi) - cos(math.atan(b_shifted)) * width_left
            y_min_2 = -b_shifted * (x_min_2 - a*cos(phi)) + a * sin(phi)
            y_max_2 = -b_shifted * (x_max_2 - a*cos(phi)) + a * sin(phi)

            if math.hypot(x_min_1, y_min_1) < math.hypot(x_min_2, y_min_2):
                tangents.append((x_min_1, y_min_1, x_max_1, y_max_1))
            else:
                tangents.append((x_min_2, y_min_2, x_max_2, y_max_2))
    return tangents


def get_flux_smart_way(data, x, y):
    """ Function returns flux at the given coordinates with taking into account
    that they can have not integer values"""
    x, y = y, x  # Take inverse coordinate grid in a its file into account
    xMax, yMax = data.shape[0], data.shape[1]
    if (x >= xMax-1) or (y >= yMax-1) or (x < 1) or (y < 1):
        return 0
    fx, ix = math.modf(x)
    fy, iy = math.modf(y)
    ix = int(ix)
    iy = int(iy)
    flx = ((1.0-fx)*(1.0-fy)*data[ix, iy] + fx*(1.0-fy)*data[ix+1, iy] +
           fy*(1.0-fx)*data[ix, iy+1] + fx*fy*data[ix+1, iy+1])
    return flx


def determine_winding_direction(x_arms, y_arms, xcen, ycen):
    """
    Function determines the winding direction of a spiral ("clockwise" or
    "counterclockwise") based of a set of points along the spiral arm
    """
    clockwise_votes = 0
    counterclockwise_votes = 0
    for idx in range(1, len(x_arms)):
        r1, phi1 = cartesian_to_polar_plain(x_arms[idx-1], y_arms[idx-1], xcen, ycen)
        r2, phi2 = cartesian_to_polar_plain(x_arms[idx], y_arms[idx], xcen, ycen)
        if r2 < r1:
            # A non-monothonuous point on spiral. Probably due to bad fit. Skipping it
            continue
        if abs(phi1 - phi2) > math.pi / 4:
            # A discontinuity in the potision angle. Probably due to going through
            # phi=0 direction. Skipping this points also
            continue
        if phi2 > phi1:
            counterclockwise_votes += 1
        if phi2 < phi1:
            clockwise_votes += 1
    if clockwise_votes > counterclockwise_votes:
        return "clockwise"
    else:
        return "counterclockwise"
