#!/usr/bin/env python

"""
Script removes low flux cuts at the end of spirals.
"""

import glob
from os import path
from os import remove


def rm(file_name):
    if path.exists(file_name):
        remove(file_name)


def cut_tails(data, noise, sigmas):
    for params_file in glob.glob(path.join(data, "arm_params_*.dat")):
        arm_color = path.splitext(path.split(params_file)[-1])[0].split("_")[-1]
        all_data = open(params_file).readlines()
        fout = open(params_file, "w")
        for idx, line in enumerate(all_data):
            if line.startswith("#"):
                # Keep header
                fout.write(line)
                continue
            peak_flux = float(line.split()[8])
            if peak_flux > (noise*sigmas):
                # Peak is bright enough, keep it
                fout.write(line)
            else:
                # Remove weak slices
                rm(path.join(data, f"slice_{arm_color}_{idx:03}.png"))
                rm(path.join(data, f"slice_{arm_color}_{idx:03}.dat"))
        fout.close()
