#! /usr/bin/env python

from os import path
import math
from collections import defaultdict
from .regions import parse_reg_file
from .regions import DS9Region
from .math_funcs import cartesian_to_polar_spiral
from .math_funcs import polar_to_cartesian
from .math_funcs import make_log_spirals
from .math_funcs import make_tangents_to_spirals
from .math_funcs import determine_winding_direction


def load_spirals(reg_file, xcen, ycen):
    if not path.exists(reg_file):
        print("File {} does not exist".format(reg_file))
        exit(0)
    regions = parse_reg_file(reg_file)
    spirals_cartesian_x = defaultdict(lambda: [])
    spirals_cartesian_y = defaultdict(lambda: [])
    widths = defaultdict(lambda: [])
    # Load cartesian coordinates of spirals from the given region file.
    # Each spiral has its own color so it is easy to distinguish them
    dist_previous = defaultdict(lambda: 0)
    for reg in regions:
        spiral_color = reg.properties["color"]
        x_coordinate = reg.params["x"]
        y_coordinate = reg.params["y"]
        dist = math.hypot(x_coordinate-xcen, y_coordinate-ycen)
        # if dist < dist_previous[spiral_color]:
        #     # Points should monotonuously diverge from the galaxy center
        #     print("Non-monotonuous point found. Skipping it...")
        #     continue
        dist_previous[spiral_color] = dist
        widths[spiral_color].append(reg.params["radius"])
        spirals_cartesian_x[spiral_color].append(x_coordinate)
        spirals_cartesian_y[spiral_color].append(y_coordinate)

    return spirals_cartesian_x, spirals_cartesian_y, widths


def connect_with_spirals(pointing_region, xcen, ycen, out_centers, out_tangents):
    # Load spirals in cartesian coordinates
    spirals_sparse_cartesian_x, spirals_sparse_cartesian_y, widths = load_spirals(pointing_region, xcen, ycen)

    # Determine the winding direction
    color = list(spirals_sparse_cartesian_x.keys())[0]
    winding = determine_winding_direction(spirals_sparse_cartesian_x[color], spirals_sparse_cartesian_y[color],
                                          xcen, ycen)
    print("Winding:", winding)
    # Convert spiral coordinates to polar
    spirals_sparse_polar = {}
    for color in spirals_sparse_cartesian_x.keys():
        r, phi, _ = cartesian_to_polar_spiral(spirals_sparse_cartesian_x[color],
                                              spirals_sparse_cartesian_y[color],
                                              xcen, ycen, winding,
                                              allow_non_monotony=True)
        spirals_sparse_polar[color] = zip(r, phi)

    # Make smooth spirals in polar coordinates
    spirals_smooth_polar = {}
    lambda_values_smooth = {}
    widths_smooth = {}
    r0_smooth = {}
    phi0_smooth = {}
    for color in spirals_sparse_polar.keys():
        res = make_log_spirals(spirals_sparse_polar[color], widths[color])
        spirals_smooth_polar[color], lambda_values_smooth[color],\
            widths_smooth[color], r0_smooth[color], phi0_smooth[color] = res

    # Convert smooth spirals to polar coordinates
    spirals_smooth_cartesian = {}
    for color in spirals_smooth_polar.keys():
        spirals_smooth_cartesian[color] = polar_to_cartesian(spirals_smooth_polar[color], xcen, ycen)

    # Compute tangents to spirals
    tangents = {}
    for color in spirals_smooth_polar.keys():
        tangents[color] = make_tangents_to_spirals(spirals_smooth_polar[color], lambda_values_smooth[color],
                                                   widths_smooth[color], r0_smooth[color], phi0_smooth[color])

    # Save spirals coordinates to a reg file
    with open(out_centers, "w") as outfile:
        outfile.truncate(0)
        for color in spirals_smooth_cartesian.keys():
            for point in spirals_smooth_cartesian[color]:
                point_region = DS9Region(region_type="point",
                                         params={"x": point[0], "y": point[1]},
                                         comment="# point=x color=%s" % color)
                outfile.write("%s\n" % point_region.to_line())

    # Save tangents to a reg file
    with open(out_tangents, "w") as outfile:
        outfile.truncate(0)
        for color in tangents.keys():
            for idx in range(len(tangents[color])):
                line = tangents[color][idx]
                lam = lambda_values_smooth[color][idx]
                pitch = math.atan(lam)
                line_region = DS9Region(region_type="line",
                                        params={"x1": line[0]+xcen, "y1": line[1]+ycen,
                                                "x2": line[2]+xcen, "y2": line[3]+ycen},
                                        comment="# line=0 0 color=%s  pitch=%1.5f" % (color, pitch))
                outfile.write("%s\n" % line_region.to_line())
