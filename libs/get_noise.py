#! /usr/bin/env python

import numpy as np
from astropy.io import fits


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def triangle_area(a, b, c):
    return ((c.x*b.y - b.x*c.y) - (c.x*a.y - a.x*c.y) + (b.x*a.y - a.x*b.y))/2


def get_noise(path_to_fits, path_to_reg):
    image_data = fits.getdata(path_to_fits)
    ySize, xSize = image_data.shape
    fluxes_in_box = []
    for line in open(path_to_reg):
        if "#" in line:
            # Remove comments line
            line = line.split("#")[0]

        if "box" in line:
            params = line[4:-2].split(",")
            cen = Point(float(params[0]),
                        float(params[1]))
            width = float(params[2])
            height = float(params[3])
            a = Point(cen.x - width/2.0, cen.y - height/2.0)
            b = Point(cen.x - width/2.0, cen.y + height/2.0)
            c = Point(cen.x + width/2.0, cen.y + height/2.0)
            d = Point(cen.x + width/2.0, cen.y - height/2.0)
            xmin = max(0, int(min(a.x, b.x, c.x, d.x)))
            xmax = min(xSize, int(max(a.x, b.x, c.x, d.x)))
            ymin = max(0, int(min(a.y, b.y, c.y, d.y)))
            ymax = min(ySize, int(max(a.y, b.y, c.y, d.y)))
            for x in range(xmin, xmax+1):
                for y in range(ymin, ymax+1):
                    p = Point(x, y)
                    t1Area = triangle_area(p, a, b)
                    t2Area = triangle_area(p, b, c)
                    t3Area = triangle_area(p, c, d)
                    t4Area = triangle_area(p, d, a)
                    sumOfSigns = (np.sign(t1Area) + np.sign(t2Area) +
                                  np.sign(t3Area) + np.sign(t4Area))
                    if abs(sumOfSigns) == 4:
                        fluxes_in_box.append(image_data[y, x])
    return np.std(fluxes_in_box)
