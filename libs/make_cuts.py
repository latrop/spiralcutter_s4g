#! /usr/bin/env python

"""
Script performs cutting of spiral arms along tangents to logarithmic
spirals defined via .reg file
"""

import math
import os
from os import path
import shelve
import subprocess
from collections import defaultdict
from collections import Counter
import json
import warnings
import numpy as np
from astropy.io import fits
from astropy.convolution import convolve_fft as fftconvolve
from scipy.ndimage import maximum_position
from scipy.optimize import fmin
from scipy.optimize import differential_evolution
from scipy.interpolate import interp1d
from scipy.integrate import simps
# from scipy.signal import fftconvolve
# from scipy.integrate import simps
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec
from .regions import parse_reg_file
from .math_funcs import get_flux_smart_way


try:
    os.environ["DISPLAY"]
except KeyError:
    matplotlib.use("Agg")

warnings.filterwarnings('ignore', '.*output shape of zoom.*')


def gaussian_lopsided(dist, params):
    # Unpack params
    amplitude = params[0]
    peak_pos = params[1]
    width_left = params[2]
    width_right = params[3]
    # Compute exponential rise and decay
    flux = np.zeros_like(dist) + amplitude
    left_idx = dist <= peak_pos
    right_idx = dist > peak_pos
    flux[left_idx] *= np.exp(-((dist[left_idx] - peak_pos)**2) / (width_left**2))
    flux[right_idx] *= np.exp(-((dist[right_idx] - peak_pos)**2) / (width_right**2))
    return flux


def fitting_function(dist, params):
    # Unpack params
    linear_slope = params[4]
    linear_intercept = params[5]
    # Compude exponential rise and decay
    flux = gaussian_lopsided(dist, params[:4])
    # Add linear slope
    flux += dist * linear_slope + linear_intercept
    return flux


def fitter_fmin(dist, flux, flux_std, psf=None, x0=None):
    def chisq(params):
        computed_flux = fitting_function(dist, params)
        if psf is not None:
            convolved_flux = fftconvolve(computed_flux, psf, mode="same")
            return np.average((flux - convolved_flux)**2, weights=1/flux_std)
        return np.average((flux - computed_flux)**2, weights=1/flux_std)
    # Find initial fitting parameters
    if x0 is None:
        amplitude = max(flux)
        max_idx = maximum_position(flux)[0]
        peak_pos = dist[max_idx]
        width_left = 0.5 * (dist[max_idx] - dist[0])
        width_right = 0.5 * (dist[-1] - dist[max_idx])
        linear_slope = (flux[-1] - flux[0]) / (dist[-1] - dist[0])
        linear_intercept = flux[0] - linear_slope * dist[0]
        params0 = [amplitude, peak_pos, width_left, width_right, linear_slope, linear_intercept]
    else:
        params0 = x0
    res = fmin(func=chisq, x0=params0, maxfun=10000, maxiter=10000, disp=False)
    min_func_value = chisq(res)
    return res, min_func_value


def fitter_de_(dist, flux, flux_std, psf=None, fwhm=None):
    def chisq(params):
        computed_flux = gaussian_lopsided(dist, params)
        if psf is not None:
            try:
                convolved_flux = fftconvolve(computed_flux, psf, boundary="fill")
            except Exception:
                # Something wrong with the PSF
                return np.average((flux - computed_flux)**2, weights=1/flux_std) * 10
            return np.average((flux - convolved_flux)**2, weights=1/flux_std)
        return np.average((flux - computed_flux)**2, weights=1/flux_std)
    # Find bounds on fitting parameters for gaussian
    amplitude = (0.5*max(flux), 10.0*max(flux))
    peak_pos = (dist[0], dist[-1])
    if fwhm is None:
        width_left = (1, 1.5*dist[-1])
        width_right = (1, 1.5*dist[-1])
    else:
        width_left = (fwhm/2.355, 1*dist[-1])
        width_right = (fwhm/2.355, 1*dist[-1])

    # Find bounds on fitting parameters for the linear slope.
    # # Let's estimate the slope based on several points in the beginning and in the end.
    # n_of_points = max(3, int(0.1 * len(dist)))
    # x1 = np.mean(dist[:n_of_points])
    # x2 = np.mean(dist[-n_of_points:])
    # y1 = np.mean(flux[:n_of_points])
    # y2 = np.mean(flux[-n_of_points:])
    # slope = (y2-y1) / (x2-x1)
    # intercept = y1
    # linear_slope = (slope/3-3, slope*3+3)
    # linear_intercept = (intercept-max(flux)*0.5, intercept+max(flux)*0.5)

    bounds = [amplitude, peak_pos, width_left, width_right]
    res = differential_evolution(func=chisq, bounds=bounds, polish=False)
    min_func_value = chisq(res.x)
    good_fit = True
    for idx in range(len(res.x)):
        bound_a = bounds[idx][0]
        bound_b = bounds[idx][1]
        param_range = bound_b - bound_a
        epsilon = 0.01 * param_range
        if ((res.x[idx] - bound_a) < epsilon) or ((bound_b - res.x[idx]) < epsilon):
            # print(f"Param {idx} is too close to bound limit")
            # print(f"{bound_a}, {res.x[idx]}, {bound_b}")
            good_fit = False
    return res.x, min_func_value, good_fit


# def fitter_de(dist, flux, flux_std, psf=None, fwhm=None):
#     def chisq(params):
#         computed_flux = gauss_with_neg_gauss(dist, params)
#         if psf is not None:
#             try:
#                 convolved_flux = fftconvolve(computed_flux, psf, boundary="fill")
#             except Exception:
#                 # Something wrong with the PSF
#                 return np.average((flux - computed_flux)**2, weights=1/flux_std) * 10
#             return np.average((flux - convolved_flux)**2, weights=1/flux_std)
#         return np.average((flux - computed_flux)**2, weights=1/flux_std)
#     # Find bounds on fitting parameters for gaussian
#     amplitude = (0.5*max(flux), 10.0*max(flux))
#     peak_pos = (dist[0], dist[-1])
#     if fwhm is None:
#         width_left = (1, 1.5*dist[-1])
#         width_right = (1, 1.5*dist[-1])
#     else:
#         width_left = (fwhm/2.355, 1*dist[-1])
#         width_right = (fwhm/2.355, 1*dist[-1])
#     neg_amplitude = (0, max(flux))

#     bounds = [amplitude, peak_pos, width_left, width_right, neg_amplitude, peak_pos, width_left]
#     res = differential_evolution(func=chisq, bounds=bounds, polish=False)
#     min_func_value = chisq(res.x)
#     good_fit = True
#     for idx in range(len(res.x)):
#         bound_a = bounds[idx][0]
#         bound_b = bounds[idx][1]
#         param_range = bound_b - bound_a
#         epsilon = 0.01 * param_range
#         if ((res.x[idx] - bound_a) < epsilon) or ((bound_b - res.x[idx]) < epsilon):
#             # print(f"Param {idx} is too close to bound limit")
#             # print(f"{bound_a}, {res.x[idx]}, {bound_b}")
#             good_fit = False
#     return res.x, min_func_value, good_fit


def get_center_of_line_region(line_region):
    x1 = line_region.params["x1"]
    y1 = line_region.params["y1"]
    x2 = line_region.params["x2"]
    y2 = line_region.params["y2"]
    x_center = x1 + 0.5*(x2 - x1)
    y_center = y1 + 0.5*(y2 - y1)
    return x_center, y_center


def make_cut(data, background_mask_data, line_region, x_orig, y_orig, ax_full_galaxy, relative_width):
    # Unpack region to find cut beginning and ending points
    x1 = line_region.params["x1"]
    y1 = line_region.params["y1"]
    x2 = line_region.params["x2"]
    y2 = line_region.params["y2"]

    # Orient slice such that it goes from the galaxy center outwards
    d1 = math.hypot(x1-x_orig, y1-y_orig)
    d2 = math.hypot(x2-x_orig, y2-y_orig)
    if d1 > d2:
        x1, y1, x2, y2 = x2, y2, x1, y1
    pitch = float(line_region.properties["pitch"])
    if x2 != x1:
        slope = (y2-y1) / (x2-x1)
    else:
        # Vertically oriented cut
        slope = 1e5
    # Find cutting line parameters
    cut_length = math.hypot(x1-x2, y1-y2)
    x_center = x1 + 0.5*(x2 - x1)
    y_center = y1 + 0.5*(y2 - y1)
    r0 = math.hypot(x_center-x_orig, y_center-y_orig)
    theta0 = math.atan2(y_center-y_orig, x_center-x_orig)
    if theta0 < 0:
        theta0 += 2 * math.pi

    # Orthogonal to a cut that goes through a starting and ending points
    slope_ortho = -1.0 / slope

    # Create a bunch of parallel cuts
    n_of_cuts = 23
    parallel_cuts = []
    cut_bounds = []  # To show them with ds9 on the image
    delta = 0.5 * relative_width * cut_length
    for idx, d in enumerate(np.linspace(-delta, delta, n_of_cuts)):
        # Center of the parallel line
        x_parallel_center = x_center + d * math.cos(math.atan(slope_ortho))
        y_parallel_center = y_center + d * math.sin(math.atan(slope_ortho))

        # Shift the center a bit to take into account the curvature of the spiral arm
        theta_parallel = math.atan2(y_parallel_center - y_orig, x_parallel_center - x_orig)
        if theta_parallel < 0:
            theta_parallel += 2 * math.pi
        delta_theta = theta_parallel - theta0
        if delta_theta > math.pi:
            delta_theta -= 2 * math.pi
        elif delta_theta < - math.pi:
            delta_theta += 2 * math.pi
        r_parallel_new = r0 * math.exp(math.tan(pitch) * delta_theta)

        # Compute a perpendicular to a spiral arm in this point
        a = r_parallel_new * math.exp(math.tan(pitch) * delta_theta)
        b = (math.tan(pitch) * math.cos(theta_parallel) - math.sin(theta_parallel)) /\
            (math.tan(pitch) * math.sin(theta_parallel) + math.cos(theta_parallel))
        x_parallel_1 = a*math.cos(theta_parallel) + math.cos(math.atan(b)) * cut_length / 2
        x_parallel_2 = a*math.cos(theta_parallel) - math.cos(math.atan(b)) * cut_length / 2
        y_parallel_1 = -b * (x_parallel_1 - a*math.cos(theta_parallel)) + a * math.sin(theta_parallel)
        y_parallel_2 = -b * (x_parallel_2 - a*math.cos(theta_parallel)) + a * math.sin(theta_parallel)

        # Determine which end of a cut is closer to the galaxy centre to make sure that all
        # cuts are properly oriented
        d_parallel_1 = math.hypot(x_parallel_1, y_parallel_1)
        d_parallel_2 = math.hypot(x_parallel_2, y_parallel_2)
        if d_parallel_1 > d_parallel_2:
            # Swap beninning and ending
            x_parallel_1, y_parallel_1, x_parallel_2, y_parallel_2 = \
                x_parallel_2, y_parallel_2, x_parallel_1, y_parallel_1
        if x_parallel_1 != x_parallel_2:
            slice_slope = (y_parallel_2-y_parallel_1) / (x_parallel_2-x_parallel_1)
        else:
            slice_slope = 1e5

        if (idx == 0) or (idx == n_of_cuts - 1):
            dx = x_parallel_2-x_parallel_1
            cut_bounds.append((x_parallel_1+x_orig, y_parallel_1+y_orig, x_parallel_1+x_orig+dx,
                               y_parallel_1+y_orig+y_parallel_2-y_parallel_1))
            if dx != 0:
                ax_full_galaxy.arrow(x_parallel_1+x_orig, y_parallel_1+y_orig, dx,
                                     y_parallel_2-y_parallel_1, color="r", head_width=0.1 * cut_length)

        def parallel_cut_flux(r, x_1=x_parallel_1, y_1=y_parallel_1, x_2=x_parallel_2, s=slice_slope):
            # This function returns the flux value at a given point on a parallel cut
            dx = r * math.cos(math.atan(s))
            dy = r * math.sin(math.atan(s))
            if x_1 > x_2:
                x = x_1 - dx + x_orig
                y = y_1 - dy + y_orig
            elif x_1 < x_2:
                x = x_1 + dx + x_orig
                y = y_1 + dy + y_orig
            else:
                x = x_1 + x_orig
                y = y_1 + r + y_orig
            return x, y

        parallel_cuts.append(parallel_cut_flux)

    def cut_line(r, x_1=x1-x_orig, y_1=y1-y_orig, x_2=x2-x_orig, s=slope):
        dx = r * math.cos(math.atan(s))
        dy = r * math.sin(math.atan(s))
        if x_1 > x_2:
            x = x_1 - dx + x_orig
            y = y_1 - dy + y_orig
        elif x_1 < x_2:
            x = x_1 + dx + x_orig
            y = y_1 + dy + y_orig
        else:
            x = x_1 + x_orig
            y = y_1 + r + y_orig
        return x, y

    r_cut = []
    flux_cut = []
    flux_cut_std = []
    for r in np.linspace(0, cut_length, int(1.5 * cut_length)):
        parallel_fluxes = []
        for cut in parallel_cuts:
            x, y = cut(r)
            # check if this pixel was masked out
            if get_flux_smart_way(background_mask_data, x, y) != 0:
                parallel_fluxes.append(np.nan)
            else:
                parallel_fluxes.append(get_flux_smart_way(data, x, y))
        if not np.isnan(parallel_fluxes).all():
            # If not all values are nans (i.e. not all pixels are masked out) we
            # can compute median flux in this cut position
            r_cut.append(r)
            flux_cut.append(np.nanmedian(parallel_fluxes))
            flux_cut_std.append(np.nanstd(parallel_fluxes))

    ax_full_galaxy.imshow(data, origin="lower", vmin=np.percentile(data, 1), vmax=np.percentile(data, 99))
    ax_full_galaxy.plot([x1, x2], [y1, y2], color="k")

    flux_cut_std = np.array(flux_cut_std)
    flux_cut_std[flux_cut_std == 0] = np.median(flux_cut_std[flux_cut_std != 0])
    return np.array(r_cut), np.array(flux_cut), flux_cut_std, cut_line, parallel_cuts, cut_bounds


def make_cuts(fitsfile, regfile, xcen, ycen, resdir, psf, fwhm, mask, pc_per_pix, relative_width):
    data = fits.getdata(fitsfile)

    if mask is not None:
        background_mask_data = fits.getdata(mask)
    else:
        background_mask_data = np.zeros_like(data)

    regions = parse_reg_file(regfile)
    max_region_width = max(regions, key=lambda x: x.size()).size()
    fig = plt.figure(figsize=(10, 10))
    image_grid = GridSpec(2, 2, figure=fig)
    x_centerline = defaultdict(lambda: [])
    y_centerline = defaultdict(lambda: [])
    left_border_x = defaultdict(lambda: [])
    left_border_y = defaultdict(lambda: [])
    right_border_x = defaultdict(lambda: [])
    right_border_y = defaultdict(lambda: [])
    slice_counter = Counter()
    for idx, reg in enumerate(regions[::2]):
        color = reg.properties["color"]
        slice_counter[color] += 1
        json_file_name = path.join(resdir, "all_slices_data.json")
        if path.exists(json_file_name):
            with open(json_file_name) as json_file:
                db = json.load(json_file)
        else:
            db = {'slices_list': []}
            db["slice_relative_width"] = relative_width
        out_png_name = path.join(resdir, f"slice_{color}_{slice_counter[color]:03}.png")
        slice_name = f"slice_{color}_{slice_counter[color]:03}"
        if (slice_name in db) and path.exists(out_png_name):
            print(f"{slice_name} already done")
            continue
        if (slice_name in db) and (db[slice_name] is None):
            print(f"{slice_name} marked as bad")
            continue
        if (slice_name in db) and (db[slice_name] is not None) and (not path.exists(out_png_name)):
            print(f"{slice_name} was removed, doing it again")
            del db[slice_name]
            db['slices_list'].remove(slice_name)
            # Remove this slice results from arm_params*.dat file
            file_name = path.join(resdir, "arm_params_%s.dat" % (color))
            if path.exists(file_name):
                old_data = open(file_name).readlines()
                fout = open(file_name, "w")
                for line in old_data:
                    if line.startswith("#"):
                        fout.write(line)
                        continue
                    if int(line.split()[11]) == int(slice_name.split("_")[-1]):
                        # The slice idx matches the line idx
                        continue
                    fout.write(line)
                fout.close()
            # Remove this slice results from borders*.dat file
            file_name = path.join(resdir, "borders_%s.dat" % (color))
            if path.exists(file_name):
                old_data = open(file_name).readlines()
                fout = open(file_name, "w")
                for line in old_data:
                    if line.startswith("#"):
                        fout.write(line)
                        continue
                    if int(line.split()[6]) == int(slice_name.split("_")[-1]):
                        # The slice idx matches the line idx
                        continue
                    fout.write(line)
                fout.close()

        while 1:
            # Untill the user is satisfied with the slice and dont send 'redo' commands
            reg_center_x, reg_center_y = get_center_of_line_region(reg)
            galactocentric_distance = np.hypot(reg_center_x-xcen, reg_center_y-ycen)
            ax_full_galaxy = fig.add_subplot(image_grid[0, 0])
            dist, flux, flux_std, cut_line, parallel_cuts, cut_bounds = make_cut(data, background_mask_data,
                                                                                 reg, xcen, ycen, ax_full_galaxy,
                                                                                 relative_width)
            len_of_cut = len(dist)
            if len_of_cut < 7:
                fig.clf()
                slice_db = None
                break

            # Store all the information we need for fitting into a shelve database
            slice_db = shelve.open("to_fit.db")
            slice_db["cut_region"] = reg.to_line()  # Region with current cut
            slice_db["dist"] = dist
            slice_db["flux"] = flux
            slice_db["flux_std"] = flux_std
            slice_db["fits_file"] = fitsfile
            slice_db["cut_bounds"] = cut_bounds
            slice_db.close()
            # Run differential evolution method to compute optimal parameters
            good_fit = True
            subprocess.call(f"./libs/interactive_cut_fitter.py {fwhm}", shell=True)
            slice_db = shelve.open("interactive_fit.db")
            if "redo" in slice_db:
                if slice_db["redo"] == "longer":
                    reg.resize(1.25)
                elif slice_db["redo"] == "shorter":
                    reg.resize(1/1.25)
                elif slice_db["redo"] == "wider":
                    relative_width *= 1.25
                elif slice_db["redo"] == "narrower":
                    relative_width /= 1.25
            else:
                break

        if (slice_db is None) or (slice_db["good"] is False):
            # Failed fit or the fit was not performed at all (e.g. due
            # to a way to short slice)
            if slice_db is not None:
                # Close if the fit was performed but not successfuly
                slice_db.close()
            plt.clf()
            print("Not good")
            db[slice_name] = None
            with open(json_file_name, "w") as json_file:
                json.dump(db, json_file)
            continue
        params_opt = slice_db["params"]
        chi_sq = slice_db["chisq"]
        full_flux_fit = slice_db["full_flux"]
        main_gaussian_fit = slice_db["main_gaussian"]
        dist_to_fit = slice_db["dist_to_fit"]
        flux_to_fit = slice_db["flux_to_fit"]
        flux_std_to_fit = slice_db["flux_std_to_fit"]
        flux_detrended = slice_db["flux_detrended"]
        flux_dedusted = slice_db["flux_dedusted"]
        flux_cleaned = slice_db["flux_cleaned"]
        trend_slope = slice_db["trend_slope"]
        trend_intercept = slice_db["trend_intercept"]
        n_of_add_gaussians = int((len(params_opt)-6) / 3)  # Number of additional gaussians in the fit
        slice_db.close()

        # Unpack fitted params
        amplitude_left_opt = params_opt[0]
        peak_position_left_opt = params_opt[1]
        width_left_opt = params_opt[2]
        trap_width_opt = params_opt[3]
        trap_slope_opt = params_opt[4]  # In radians
        width_right_opt = params_opt[5]
        # Find x and y coordinates of the geometric center
        fitted_geometric_center = peak_position_left_opt - width_left_opt/2 + trap_width_opt / 2 + width_right_opt / 2
        x_geom_cen, y_geom_cen = cut_line(fitted_geometric_center)
        # Find x and y coordinates of the left border
        x_left, y_left = cut_line(peak_position_left_opt - width_left_opt)
        # Find x and y coordinates of the right border
        x_right, y_right = cut_line(peak_position_left_opt + trap_width_opt + width_right_opt)
        # Save peak, left and right coordinates to plot them all over the image of the galaxy
        x_centerline[color].append(x_geom_cen)
        y_centerline[color].append(y_geom_cen)
        left_border_x[color].append(x_left)
        left_border_y[color].append(y_left)
        right_border_x[color].append(x_right)
        right_border_y[color].append(y_right)

        # Compute center of mass of the main gaussian
        gauss_center_of_mass = simps(main_gaussian_fit*dist_to_fit, dist_to_fit) / simps(main_gaussian_fit, dist_to_fit)
        x_gauss_center_of_mass, y_gauss_center_of_mass = cut_line(gauss_center_of_mass)
        # Compute center of mass of the entire slice
        full_center_of_mass = simps(flux_to_fit*dist_to_fit, dist_to_fit) / simps(flux_to_fit, dist_to_fit)
        x_full_center_of_mass, y_full_center_of_mass = cut_line(full_center_of_mass)
        # Compute center of mass for flux corrected for dust attenuation
        dedusted_center_of_mass = simps(flux_dedusted*dist_to_fit, dist_to_fit) / simps(flux_dedusted, dist_to_fit)
        x_dedusted_center_of_mass, y_dedusted_center_of_mass = cut_line(dedusted_center_of_mass)
        # Compute integral asymmetry around the geometric center
        idx_left = dist_to_fit < fitted_geometric_center
        idx_right = dist_to_fit > fitted_geometric_center
        integral_left = simps(main_gaussian_fit[idx_left], dist_to_fit[idx_left])
        integral_right = simps(main_gaussian_fit[idx_right], dist_to_fit[idx_right])
        asymmetry = integral_left / integral_right

        # Save all slice info into a single file with another slices
        data_to_save = {}
        data_to_save["ds9_region"] = reg.to_line()
        data_to_save["slice_relative_width"] = relative_width
        data_to_save['main_gauss_params'] = list(params_opt[:6])
        data_to_save['n_of_add_gaussians'] = n_of_add_gaussians
        data_to_save['add_gauss_params'] = [list(params_opt[6+i*3: 6+i*3+3]) for i in range(n_of_add_gaussians)]
        data_to_save['dist_pc'] = list(dist_to_fit*pc_per_pix)
        data_to_save['trend_slope'] = trend_slope
        data_to_save['trend_intercept'] = trend_intercept
        data_to_save['flux'] = list(flux_to_fit)
        data_to_save['flux_std'] = list(flux_std_to_fit)
        data_to_save['flux_dedusted'] = list(flux_dedusted)
        data_to_save['flux_cleaned'] = list(flux_cleaned)
        data_to_save['dist_pix'] = list(dist_to_fit)
        data_to_save['full_center_of_mass_pix'] = full_center_of_mass
        data_to_save['full_center_of_mass_pc'] = full_center_of_mass*pc_per_pix
        data_to_save['x_full_center_of_mass_pix'] = x_full_center_of_mass
        data_to_save['y_full_center_of_mass_pix'] = y_full_center_of_mass
        data_to_save['gauss_center_of_mass_pix'] = gauss_center_of_mass
        data_to_save['gauss_center_of_mass_pc'] = gauss_center_of_mass*pc_per_pix
        data_to_save['x_gauss_center_of_mass_pix'] = x_gauss_center_of_mass
        data_to_save['y_gauss_center_of_mass_pix'] = y_gauss_center_of_mass
        data_to_save['dedusted_center_of_mass_pix'] = dedusted_center_of_mass
        data_to_save['dedusted_center_of_mass_pc'] = dedusted_center_of_mass*pc_per_pix
        data_to_save['x_dedusted_center_of_mass_pix'] = x_dedusted_center_of_mass
        data_to_save['y_dedusted_center_of_mass_pix'] = y_dedusted_center_of_mass
        data_to_save['x_geom_cen_pix'] = x_geom_cen
        data_to_save['y_geom_cen_pix'] = y_geom_cen
        data_to_save['x_left_pix'] = x_left
        data_to_save['y_left_pix'] = y_left
        data_to_save['x_right_pix'] = x_right
        data_to_save['y_right_pix'] = y_right
        data_to_save['x_range_min_pix'], data_to_save['y_range_min_pix'] = cut_line(dist_to_fit[0])
        data_to_save['x_range_max_pix'], data_to_save['y_range_max_pix'] = cut_line(dist_to_fit[-1])
        data_to_save['asymmetry'] = asymmetry
        # Let's compute the distances of some points on the arm to the galaxy center, so we
        # can use them in the future to find the distance between an arbitrary point on the arm
        # and the galaxy center
        r_on_slice_pc = []
        distance_to_galaxy_center_pc = []
        for r in dist_to_fit:
            x, y = cut_line(r)
            r_on_slice_pc.append(r * pc_per_pix)
            distance_to_galaxy_center_pc.append(np.hypot(xcen-x, ycen-y) * pc_per_pix)
        data_to_save['r_on_slice_pc'] = list(np.array(r_on_slice_pc))
        data_to_save['distance_to_galaxy_center_pc'] = list(np.array(distance_to_galaxy_center_pc))
        db[slice_name] = data_to_save
        # Add descriptrion to the db file
        if "help" not in db.keys():
            # Save slice data points to a text file
            help_text = """
            The information about each slice is stored in a dictionary form that can be accessed by
            'slice_*color*_*idx*' key (which is exactly the same as the name of a png image with the slice).
            example: db['slice_green_001']['dist_pc']
            The dictionary keys are:
                ds9_region -- ds9 region on the image along which the slice was made
                slice_relative_width -- slice relative width
                dist_pc -- distance values along slice in parsecs;
                dist_pix -- distance values along slice in pixels;
                flux -- flux along slice (detrended);
                flux_std -- flux variation in slice;
                flux_dedusted -- flux corrected for negative gaussians;
                flux_cleaned -- all additional gaussians were removed
                main_gauss_params -- list with parameters of the main gaussian (amplitude, peak
                position, width left and width rigth), all sizes are in pixels;
                n_of_add_gaussians -- number of additional gaussianls;
                add_gauss_params -- parameters of additional gaussians (three values for each
                                     gaussian: amplitude, peak position and with). Sizes are in pixels;
                full_center_of_mass_pix -- location of a full center of mass (computed over full slice flux)
                                           in slice (pixels);
                full_center_of_mass_pc -- the same but in parsecs
                x_full_center_of_mass_pix, y_full_center_of_mass_pix -- image pixel coordinates of a full center
                              of mass (center of mass computed over the entire flux curve);
                gauss_center_of_mass_pix -- location of a center of mass computed over flux of a main gaussian (pixels);
                gauss_center_of_mass_pc -- the same but in parsecs
                x_gauss_center_of_mass_pix, y_gauss_center_of_mass_pix -- image pixel coordinates of a center
                              of mass computed over the main gaussian flux;
                dedusted_center_of_mass_pix -- location of a center of mass computed over dedusted flux (pixels);
                dedusted_center_of_mass_pc -- the same but in parsecs
                x_dedusted_center_of_mass_pix, y_dedusted_center_of_mass_pix -- image pixel coordinates of a center
                              of mass computed over the dedusted flux;
                x_peak_pix, y_peak_pix -- image coordinates of main gaussian peak position;
                x_left_pix, y_left_pix -- image coordinates of inner half-width of a spiral;
                x_right_pix, y_right_pix -- image coordinates of outer half-width of a spiral;
                x_range_min_pix, y_range_min_pix -- image coordinates of the minimal fit range
                x_range_max_pix, y_range_max_pix -- image coordinates of the minimal fit range
                r_on_slice_pc and distance_to_galaxy_center_pc is a pair of arrays to calculate (by interpolation) the
                               distance between an arbitrary point on a slice and the galaxy center (in pc)

            Also check out db['slices_list']. It contains the list of slices names
            """
            db["help"] = help_text

        # Save slice key to a list
        db['slices_list'].append(slice_name)

        # Save data to a json file
        with open(json_file_name, "w") as json_file:
            json.dump(db, json_file)

        # Some distances are masked out, but we still want to show fit results for them,
        # so let's define dist_to_show array that contains all the distances
        dist_to_show = np.arange(dist[0], dist[-1])

        flux_fitted_not_convolved = gaussian_lopsided(dist_to_show, params_opt)
        peak_flux = max(flux_fitted_not_convolved)

        ax_full_galaxy.plot(x_centerline[color], y_centerline[color], "ro", markersize=3)
        ax_full_galaxy.plot(left_border_x[color], left_border_y[color], color="m", linewidth=2)
        ax_full_galaxy.plot(right_border_x[color], right_border_y[color], color="b", linewidth=2)
        ax_full_galaxy.set_xticks([])
        ax_full_galaxy.set_yticks([])

        # Show zoomed regions around the cut
        ax_zoom_galaxy = fig.add_subplot(image_grid[0, 1])
        x1 = reg.params["x1"]
        x2 = reg.params["x2"]
        y1 = reg.params["y1"]
        y2 = reg.params["y2"]
        cutout_size = max(abs(x1-x2), abs(y1-y2))
        data_size_y, data_size_x = data.shape
        x_start = int(max(0, reg_center_x-cutout_size))
        x_end = int(min(data_size_x, reg_center_x+cutout_size))
        y_start = int(max(0, reg_center_y-cutout_size))
        y_end = int(min(data_size_y, reg_center_y+cutout_size))
        zoom_cutout = data[y_start:y_end, x_start:x_end]
        ax_zoom_galaxy.imshow(zoom_cutout, origin="lower", vmin=np.percentile(zoom_cutout, 1),
                              vmax=np.percentile(zoom_cutout, 99), extent=[x_start, x_end, y_start, y_end])
        ax_zoom_galaxy.set_xticks([])
        ax_zoom_galaxy.set_yticks([])
        ax_zoom_galaxy.plot([x1, x2], [y1, y2], color="r")
        x, y = cut_line(dist_to_fit[0])
        ax_zoom_galaxy.scatter([x], [y], color="k")
        x, y = cut_line(dist_to_fit[-1])
        ax_zoom_galaxy.scatter([x], [y], color="k")
        ax_zoom_galaxy.plot([x_geom_cen], [y_geom_cen], marker="^", color="r", markersize=10)
        ax_zoom_galaxy.plot([x_full_center_of_mass], [y_full_center_of_mass], marker="*",
                            markerfacecolor="k", markeredgecolor="k", markersize=10)
        ax_zoom_galaxy.plot([x_gauss_center_of_mass], [y_gauss_center_of_mass], marker="*",
                            markerfacecolor="b", markeredgecolor="k", markersize=10)
        ax_zoom_galaxy.plot([x_dedusted_center_of_mass], [y_dedusted_center_of_mass], marker="*",
                            markerfacecolor="cyan", markeredgecolor="k", markersize=10)
        # Show slice fit
        ax_slice_fit = fig.add_subplot(image_grid[1, :])
        # All distances on the plot should be in pc
        ax_slice_fit.set_xlim(0, pc_per_pix*max_region_width)
        ax_slice_fit.set_ylim(0, 1.25*max(flux_detrended))
        ax_slice_fit.plot(pc_per_pix*dist, flux_detrended, "go", label="Slice data")
        ax_slice_fit.fill_between(x=pc_per_pix*dist, y1=flux_detrended-flux_std,
                                  y2=flux_detrended+flux_std, color="0.80")
        ax_slice_fit.plot(pc_per_pix*dist_to_fit, full_flux_fit, linewidth=2, color="m", label="Full fit")
        ax_slice_fit.plot(pc_per_pix*dist_to_fit, main_gaussian_fit, linestyle="--", color="r", linewidth=2,
                          label="Main gaussian")

        # Mark position of half width distance from the peak
        main_gaussian_fit_interpolated = interp1d(pc_per_pix*dist_to_fit, main_gaussian_fit,
                                                  bounds_error=False, fill_value=(0, 0))
        x = pc_per_pix*(peak_position_left_opt-width_left_opt)
        y = main_gaussian_fit_interpolated(x)
        ax_slice_fit.plot(x, y, marker="^", markerfacecolor="w", markeredgecolor="k", linestyle="",
                          markersize=15, label="Half width distance from the peak")
        x = pc_per_pix*(peak_position_left_opt+trap_width_opt+width_right_opt)
        y = main_gaussian_fit_interpolated(x)
        ax_slice_fit.plot(x, y, marker="^", markerfacecolor="w", markeredgecolor="k", linestyle="",
                          markersize=15)

        # Mark peak position
        x = pc_per_pix*(peak_position_left_opt - width_left_opt/2 + trap_width_opt / 2 + width_right_opt / 2)
        y = main_gaussian_fit_interpolated(x)
        ax_slice_fit.plot(x, y, "r^", markersize=15, label="Geometric center of mass")

        # Mark gaussian center of mass position
        x = pc_per_pix*gauss_center_of_mass
        y = main_gaussian_fit_interpolated(x)
        ax_slice_fit.plot(x, y, marker="*", markerfacecolor="blue", markersize=20, markeredgecolor="k",
                          label="Gauss center of mass position", linestyle="")

        # Mark full center of mass position
        x = pc_per_pix*full_center_of_mass
        y = main_gaussian_fit_interpolated(x)
        ax_slice_fit.plot(x, y, marker="*", markerfacecolor="k", markersize=20, markeredgecolor="k",
                          label="Full center of mass position", linestyle="")

        # Mark dedusted center of mass position
        x = pc_per_pix*dedusted_center_of_mass
        y = main_gaussian_fit_interpolated(x)
        ax_slice_fit.plot(x, y, marker="*", markerfacecolor="cyan", markersize=20, markeredgecolor="k",
                          label="Dedusted center of mass position", linestyle="")

        ax_slice_fit.set_xlabel("D[pc]")
        ax_slice_fit.legend()

        fig.tight_layout()
        print("Saving figure")
        fig.savefig(path.join(resdir, f"slice_{color}_{slice_counter[color]:03}.png"))
        # plt.show()
        fig.clf()

        # Save text parameter
        file_name = path.join(resdir, "arm_params_%s.dat" % (color))
        if path.exists(file_name):
            fout = open(file_name, "a")
        else:
            # Fill up the header if there is no file yet
            fout = open(file_name, "w")
            fout.write("# dist[pix]   dist[pc]   x_geom_cen[pix]    y_geom_cen[pix]   width_left[pix]    ")
            fout.write("width_left[pc]   width_right[pix]  width_right[pc]    peak_flux    ")
            fout.write("good_fit asymmetry   slice_idx\n")
        fout.write("%1.2f  %1.2f  %1.2f  %1.2f  " % (galactocentric_distance,
                                                     galactocentric_distance * pc_per_pix, x_geom_cen, y_geom_cen))
        fout.write("%1.2f  %1.2f  " % (width_left_opt+trap_width_opt/2, pc_per_pix * (width_left_opt+trap_width_opt/2)))
        fout.write("%1.2f  %1.2f  " % (width_right_opt+trap_width_opt/2, pc_per_pix * (width_right_opt+trap_width_opt/2)))
        fout.write("%1.5e   " % peak_flux)
        if good_fit is True:
            fout.write("1 ")
        else:
            fout.write("0 ")

        fout.write("%1.2f  " % asymmetry)
        fout.write("%i\n" % int(slice_name.split("_")[-1]))
        fout.close()

        # Sort this file by the slice idx
        inds = np.genfromtxt(file_name, usecols=(11, ))
        inds_sorted = np.argsort(inds)
        old_data = open(file_name).readlines()
        fout = open(file_name, "w")
        # write header
        fout.write(old_data[0])
        # Write slices in the order
        for i in inds_sorted:
            fout.write(old_data[i+1])
        fout.close()

        # Save borders to an ascii file
        file_name = path.join(resdir, "borders_%s.dat" % color)
        if path.exists(file_name):
            fout = open(file_name, "a")
        else:
            fout = open(file_name, "w")
            fout.write("# center_line_x  center_line_y  left_border_x  left_border_y   ")
            fout.write("right_border_x   right_border_y  slice_idx\n")
        fout.write("%1.2f  " % x_geom_cen)
        fout.write("%1.2f  " % y_geom_cen)
        fout.write("%1.2f  " % x_left)
        fout.write("%1.2f  " % y_left)
        fout.write("%1.2f  " % x_right)
        fout.write("%1.2f  " % y_right)
        fout.write("%i\n" % int(slice_name.split("_")[-1]))
        fout.close()

        # Sort this file by the slice idx
        inds = np.genfromtxt(file_name, usecols=(6, ))
        inds_sorted = np.argsort(inds)
        old_data = open(file_name).readlines()
        fout = open(file_name, "w")
        # write header
        fout.write(old_data[0])
        # Write slices in the order
        for i in inds_sorted:
            fout.write(old_data[i+1])
        fout.close()
