#! /usr/bin/env python

import math
from astropy.io import fits
import numpy as np
from scipy.ndimage import zoom
from scipy.integrate import simps
from astropy.modeling import models, fitting


def get_flux_smart_way(data, x, y):
    """ Function returns flux at the given coordinates with taking into account
    that they can have not integer values"""
    x, y = y, x  # Take inverse coordinate grid in a its file into account
    xMax, yMax = data.shape[0], data.shape[1]
    if (x >= xMax-1) or (y >= yMax-1) or (x < 1) or (y < 1):
        return 0
    fx, ix = math.modf(x)
    fy, iy = math.modf(y)
    ix = int(ix)
    iy = int(iy)
    flx = ((1.0-fx)*(1.0-fy)*data[ix, iy] + fx*(1.0-fy)*data[ix+1, iy] +
           fy*(1.0-fx)*data[ix, iy+1] + fx*fy*data[ix+1, iy+1])
    return flx


def make_1d_psf(psf_fits_file, out_name, psf_oversample):
    data = fits.getdata(psf_fits_file)
    data = zoom(input=data, zoom=psf_oversample)
    y_size, x_size = data.shape
    x_center = x_size / 2 - 0.5
    y_center = y_size / 2 - 0.5
    dists = np.arange(0, x_size, 0.25)
    intens = []
    for x in dists:
        intens.append(get_flux_smart_way(data, x, y_center))
    intens = np.array(intens)
    full_flux = simps(intens, dists)
    intens /= full_flux
    dists -= x_center
    np.savetxt(out_name, np.c_[dists, intens])

    # Determine FWHM
    fit_g = fitting.LevMarLSQFitter()
    g_init = models.Gaussian1D(amplitude=np.max(intens), mean=0, stddev=1.5)
    gaussian = fit_g(g_init, dists, intens)
    fwhm = 2.355 * gaussian.stddev.value
    return fwhm
