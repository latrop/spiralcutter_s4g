#!/usr/bin/env python

import argparse
import glob
from os import path
from os import remove
import os
import math
import numpy as np
from scipy.signal import savgol_filter
from scipy.interpolate import interp1d
from scipy import stats
import matplotlib.pyplot as plt
from astropy.io import fits

from .math_funcs import make_log_spirals_asymm
from .math_funcs import determine_winding_direction
from .math_funcs import cartesian_to_polar_spiral

import matplotlib
try:
    os.environ["DISPLAY"]
except KeyError:
    matplotlib.use("Agg")


def polar_to_cartesian(r_list, phi_list, xcen, ycen):
    x_list = r_list * np.cos(phi_list) + xcen
    y_list = r_list * np.sin(phi_list) + ycen
    return x_list, y_list


def smooth_by_linear_fit(x, y):
    slope, intercept, *_ = stats.linregress(x, y)
    return lambda x: x * slope + intercept


def smooth_spirals(img_file, data, xcen, ycen, pc_per_pix):
    plt.axes().set_aspect('equal', 'datalim')
    files_with_arm_params = glob.glob(path.join(data, "arm_params*.dat"))
    image_y_size, image_x_size = fits.getdata(img_file).shape
    # Remove all existing files with smoothed data
    for fname in glob.glob(path.join(data, "*smooth*")):
        remove(fname)

    # smoothe data
    for arm_file in files_with_arm_params:
        arm_color = path.splitext(path.basename(arm_file))[0].split("_")[-1]
        # Load data
        dists, x_peaks, y_peaks, widths_left, widths_right, \
            peak_flux, good_fit = np.genfromtxt(arm_file, unpack=True, usecols=[0, 2, 3, 4, 6, 8, 9])
        good_inds = np.where(good_fit == 1)
        if len(good_inds[0]) > 7:
            dists = dists[good_inds]
            x_peaks = x_peaks[good_inds]
            y_peaks = y_peaks[good_inds]
            widths_left = widths_left[good_inds]
            widths_right = widths_right[good_inds]
            peak_flux = peak_flux[good_inds]

        # Determine the winding direction and convert spiral arms into polar coordinates
        winding = determine_winding_direction(x_peaks, y_peaks, xcen, ycen)
        r_peaks, phi_peaks, _ = cartesian_to_polar_spiral(x_peaks, y_peaks, xcen, ycen,
                                                          winding, allow_non_monotony=True)
        u_peaks = np.log(r_peaks)

        # Smooth spirals
        phi_range = max(phi_peaks) - min(phi_peaks)
        delta_phi_mean = phi_range / len(phi_peaks)
        smoothing_window = min(0.5 * math.pi, phi_range)
        window_length = max(int(smoothing_window / delta_phi_mean), 9)
        if window_length >= min(len(widths_left), len(u_peaks)):
            window_length = min(len(widths_left), len(u_peaks)) - 1
        if window_length % 2 == 0:
            window_length += 1
        polyorder = min(3, window_length-1)
        widths_left_smoothed = savgol_filter(widths_left, window_length=window_length, polyorder=polyorder)
        widths_right_smoothed = savgol_filter(widths_right, window_length=window_length, polyorder=polyorder)
        u_peaks_smoothed = savgol_filter(u_peaks, window_length=window_length, polyorder=polyorder)
        r_peaks_smoothed = np.exp(u_peaks_smoothed)
        x_peaks_smoothed, y_peaks_smoothed = polar_to_cartesian(r_peaks_smoothed, phi_peaks, xcen, ycen)
        # peak_flux_smoothed = [get_flux_smart_way(image, x, y) for x, y in zip(x_peaks_smoothed, y_peaks_smoothed)]
        peak_flux_smoothed = savgol_filter(peak_flux, window_length=window_length, polyorder=polyorder)

        # Interpolate widths
        widths_left_interpolated = interp1d(dists, widths_left_smoothed, kind="linear", bounds_error=False,
                                            fill_value=(widths_left_smoothed[0], widths_left_smoothed[-1]))
        widths_right_interpolated = interp1d(dists, widths_right_smoothed, kind="linear", bounds_error=False,
                                             fill_value=(widths_right_smoothed[0], widths_right_smoothed[-1]))

        # Put some points between smoothed spirals
        (spirals_fine_polar, lambda_values_fine, widths_left_fine, widths_right_fine, r0_fine, phi0_fine) =\
            make_log_spirals_asymm(zip(r_peaks_smoothed, phi_peaks), widths_left_smoothed, widths_right_smoothed)

        file_name = path.join(data, "smoothed_arm_params_%s.dat" % arm_color)
        if path.exists(file_name):
            fout = open(file_name, "a")
        else:
            # Fill up the header if there is no file yet
            fout = open(file_name, "w")
            fout.write("# dist[pix]   dist[pc]   x_peak[pix]    y_peak[pix]   width_left[pix]    ")
            fout.write("width_left[pc]   width_right[pix]  width_right[pc]    peak_flux\n")
        for idx in range(len(r_peaks_smoothed)):
            r = r_peaks_smoothed[idx]
            fout.write("%1.2f  %1.2f   " % (r, pc_per_pix*r))
            fout.write("%1.2f  %1.2f   " % (x_peaks_smoothed[idx], y_peaks_smoothed[idx]))
            fout.write("%1.2f  %1.2f  " % (widths_left_interpolated(r), pc_per_pix*widths_left_interpolated(r)))
            fout.write("%1.2f  %1.2f  " % (widths_right_interpolated(r), pc_per_pix*widths_right_interpolated(r)))
            fout.write("%1.2f\n" % peak_flux_smoothed[idx])
        fout.close()

        # Add the arm on the plot
        plt.plot(x_peaks, y_peaks, color=arm_color, marker="o", linestyle="")
        plt.plot(x_peaks_smoothed, y_peaks_smoothed, color=arm_color)
    plt.scatter(xcen, ycen, marker="x")
    plt.savefig(path.join(data, "arms_smoothing.png"))
    plt.clf()
